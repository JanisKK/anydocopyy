CREATE TABLE `Attachments` (
  `AttachmentID` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Type` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `Content` mediumblob NOT NULL,
  `TaskID` int(11) NOT NULL,
  `Size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Attachments`
--

INSERT INTO `Attachments` (`AttachmentID`, `Name`, `Type`, `Content`, `TaskID`, `Size`) VALUES
(40, 'Chgfapture.JPG', 'image/jpeg', ....)
-- --------------------------------------------------------

--
-- Table structure for table `ItemList`
--

CREATE TABLE `ItemList` (
  `ItemID` int(11) NOT NULL,
  `ListID` int(11) NOT NULL,
  `Nosaukums` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Piezīme` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Priority` tinyint(1) NOT NULL,
  `Timee` date NOT NULL,
  `Complete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ItemList`
--

INSERT INTO `ItemList` (`ItemID`, `ListID`, `Nosaukums`, `Piezīme`, `Priority`, `Timee`, `Complete`) VALUES
(28, 23, 'Atspiesties 100 reizes', 'Šī ir piezīme', 1, '2017-07-09', 0),
(29, 23, '100 pietupieni', 'Ja vari tad vairāk', 0, '2017-07-10', 1),
(31, 23, 'Atspiesties 500 reizes', '', 0, '2017-08-09', 0),
(35, 23, 'Free day', 'Today is that day!', 1, '2017-07-11', 0),
(36, 25, 'Ārsts', 'This is note for id36', 1, '2017-07-17', 0),
(37, 25, 'Sakārto istabu', '', 0, '2017-07-10', 1),
(38, 25, 'Vakariņas Liepājā', '', 0, '2017-07-11', 1),
(39, 23, 'Take a break', '', 0, '2017-07-13', 1),
(40, 23, 'gfh', '', 0, '2017-07-14', 1),
(42, 23, 'Piecelies', '', 0, '2017-07-15', 0),
(45, 23, 'Test', '', 0, '2017-07-14', 0),
(53, 25, 'Nothing special', 'Nothing to write here\r\n', 0, '2017-08-26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SubItems`
--

CREATE TABLE `SubItems` (
  `ID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `Nosaukums` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Complete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SubItems`
--

INSERT INTO `SubItems` (`ID`, `ItemID`, `Nosaukums`, `Complete`) VALUES
(12, 28, '80 Min', 0),
(13, 32, '70 Ja vari', 1),
(14, 30, '115 Min', 0),
(15, 36, 'Hello', 1),
(17, 36, 'This is subtask', 0);

-- --------------------------------------------------------

--
-- Table structure for table `TaskList`
--

CREATE TABLE `TaskList` (
  `TaskID` int(11) NOT NULL,
  `Nosaukums` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TaskList`
--

INSERT INTO `TaskList` (`TaskID`, `Nosaukums`) VALUES
(25, 'Stuff'),
(40, 'Hello');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Attachments`
--
ALTER TABLE `Attachments`
  ADD PRIMARY KEY (`AttachmentID`);

--
-- Indexes for table `ItemList`
--
ALTER TABLE `ItemList`
  ADD PRIMARY KEY (`ItemID`),
  ADD KEY `Relation` (`ListID`);

--
-- Indexes for table `SubItems`
--
ALTER TABLE `SubItems`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `TaskList`
--
ALTER TABLE `TaskList`
  ADD PRIMARY KEY (`TaskID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Attachments`
--
ALTER TABLE `Attachments`
  MODIFY `AttachmentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `ItemList`
--
ALTER TABLE `ItemList`
  MODIFY `ItemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `SubItems`
--
ALTER TABLE `SubItems`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `TaskList`
--
ALTER TABLE `TaskList`
  MODIFY `TaskID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
